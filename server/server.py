# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from config import ORIGINS_WHITELIST, APP_VERSION, API_PREFIX
from routes import create_base_router, create_routers
from database import initialize_database_metadata
from relations import init as initialize_relations

from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse
from pydantic import BaseModel


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=ORIGINS_WHITELIST,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


class ApiVersion(BaseModel):
    version: str


initialize_database_metadata()
initialize_relations()

def add_static_routes(app):
    index_file = '../client/dist/index.html'

    for index_path in ['/', '/login', '/restricted', '/e', '/s', '/p', '/u']:

        @app.get(index_path)
        async def _access_index():
            return FileResponse(index_file)

    for index_path in ['/e/{id}', '/p/{id}', '/s/{id}', '/u/{id}']:

        @app.get(index_path)
        async def _access_index(id):
            return FileResponse(index_file)

    app.mount('/', StaticFiles(directory='../client/dist'), name='frontend')


if API_PREFIX != '':

    inner_app = FastAPI()

    @inner_app.get('/version', response_model=ApiVersion)
    async def get_version() -> dict:
        return {'version': APP_VERSION}

    create_base_router(inner_app)
    create_routers(inner_app)

    app.mount(API_PREFIX, inner_app)

else:

    @app.get('/version', response_model=ApiVersion)
    async def get_version() -> dict:
        return {'version': APP_VERSION}

    create_base_router(app)
    create_routers(app)

add_static_routes(app)
