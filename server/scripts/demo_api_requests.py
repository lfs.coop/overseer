# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import os
from dotenv import load_dotenv
load_dotenv()

import requests

api_host = os.environ.get('API_HOST', 'localhost')
api_port = os.environ.get('API_PORT', 5500)
api_prefix = os.environ.get('API_PREFIX', '')
BASE_URL = f'http://{api_host}:{api_port}{api_prefix}'
CONFIG = { 'headers': None }


def get_token(data: dict) -> str:
    resp = requests.post(BASE_URL + '/token', data=data)
    return resp.json()['access_token']

def api_get(route, json):
    resp = requests.get(BASE_URL + route, json=json, headers=CONFIG['headers'])
    return resp.json()
def api_post(route, json):
    resp = requests.post(BASE_URL + route, json=json, headers=CONFIG['headers'])
    return resp.json()
def api_patch(route, json):
    resp = requests.patch(BASE_URL + route, json=json, headers=CONFIG['headers'])
    return resp.json()


if __name__ == '__main__':
    token = get_token({'username': '***', 'password': '***'})
    CONFIG['headers'] = {'Authorization': 'Bearer {}'.format(token)}

    # link a user to a site
    user = api_patch('/users-link', json={
        'user_login': 'guest',
        'site': '***', #uid
    })
    print(user)
