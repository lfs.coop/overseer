# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import argparse
import json
from dotenv import load_dotenv
load_dotenv()

# from helpers.utils import HAS_RELATIONSHIP_PROPERTIES, RETURN_RELATIONSHIP_PROPERTIES, RETURN_RELATIONSHIP_PK_ONLY
# print(HAS_RELATIONSHIP_PROPERTIES, RETURN_RELATIONSHIP_PROPERTIES, RETURN_RELATIONSHIP_PK_ONLY)
# print('\n*****__\n')

from database import db, session_scope, initialize_database_metadata
from relations import init as initialize_relations

initialize_database_metadata()
initialize_relations()

from models.extension import ExtensionInDB
from models.project import ProjectInDB
from models.site import SiteInDB
from models.user import UserInDB
from models.version import VersionInDB
from helpers.tools import get_connection_prop_name, connect
from helpers.utils import create_uid
from security import hash_password


def drop_db():
    print('Dropping database')
    db.run('MATCH (n) DETACH DELETE n')

def create_entities(db_schema, data):
    instances = []
    for item in data:
        item['uid'] = create_uid()
        instance = db_schema(**item)
        with session_scope() as session:
            session.create(instance)
        instances.append(instance.to_json())
    return instances

def create_extensions(data):
    return create_entities(ExtensionInDB, data)

def create_projects(data):
    return create_entities(ProjectInDB, data)

def create_sites(data):
    return create_entities(SiteInDB, data)

def create_users(data):
    return create_entities(UserInDB, data)

def create_versions(data):
    return create_entities(VersionInDB, data)

def link_user_to_site(user_uid, site_uid):
    user = UserInDB.find_one(uid=user_uid)
    if not user: print('Error: could not find user with uid <%s>' % user_uid)
    site = SiteInDB.find_one(uid=site_uid)
    if not site: print('Error: could not find site with uid <%s>' % site_uid)
    rel_name = get_connection_prop_name('sites', 'SiteInDB')
    getattr(user, rel_name).add(site)
    with session_scope() as session:
        session.push(user)

def link_project_to_site(project_uid, site_uid):
    project = ProjectInDB.find_one(uid=project_uid)
    if not project: print('Error: could not find project with uid <%s>' % project_uid)
    site = SiteInDB.find_one(uid=site_uid)
    if not site: print('Error: could not find site with uid <%s>' % site_uid)
    rel_name = get_connection_prop_name('projects', 'ProjectInDB')
    getattr(site, rel_name).add(project)
    with session_scope() as session:
        session.push(site)

def link_version_to_extension(version_uid, extension_uid):
    version = VersionInDB.find_one(uid=version_uid)
    if not version: print('Error: could not find version with uid <%s>' % version_uid)
    extension = ExtensionInDB.find_one(uid=extension_uid)
    if not extension: print('Error: could not find extension with uid <%s>' % extension_uid)
    rel_name = get_connection_prop_name('versions', 'VersionInDB')
    getattr(extension, rel_name).add(version)
    with session_scope() as session:
        session.push(extension)

def set_version_as_ext_default(version_uid, extension_uid):
    version = VersionInDB.find_one(uid=version_uid)
    if not version: print('Error: could not find version with uid <%s>' % version_uid)
    extension = ExtensionInDB.find_one(uid=extension_uid)
    if not extension: print('Error: could not find extension with uid <%s>' % extension_uid)
    connect(extension, version, 'default_version')
    with session_scope() as session:
        session.push(extension)

def add_extension_to_project(version_uid, project_uid, type, **rel_data):
    version = VersionInDB.find_one(uid=version_uid)
    if not version: print('Error: could not find version with uid <%s>' % version_uid)
    project = ProjectInDB.find_one(uid=project_uid)
    if not project: print('Error: could not find project with uid <%s>' % project_uid)
    rel_name = get_connection_prop_name('%s_extensions' % type, 'VersionInDB')
    getattr(project, rel_name).add(version, **rel_data)
    with session_scope() as session:
        session.push(project)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--drop', action='store_true')
    args = parser.parse_args()

    if args.drop:
        drop_db()

    users = create_users([
        { 'login': 'colibri', 'code': 'colibri', 'name': 'Colibri', 'is_enabled': True, 'description': '' },
    ])

    sites = create_sites([
        { 'code': 'LFS', 'name': 'Les Fées Spéciales', 'type': 'studio', 'is_enabled': True, 'description': '' },
        { 'code': 'mina-remote', 'name': 'Mina Remote', 'type': 'user', 'is_enabled': True, 'description': '' },
    ])

    projects = create_projects([
        { 'name': 'Project 1', 'description': 'A first project', 'is_enabled': True, 'is_archived': False,
          'libreflow': '0.1.0',
          'redis_url': '://redis', 'redis_password': 'redis_password', 'redis_port': 6380, 'redis_cluster': '<cluster>', 'redis_db': 0,
          'mongo_url': '://mongo', 'mongo_user': 'mongo', 'mongo_password': 'mongo_password',
          'exchange_server_type': 'minio', 'exchange_server_url': '://exch_server', 'exchange_server_login': 'exch_server_login', 'exchange_server_password': 'exchange_server_password', 'exchange_server_bucket': '<bucket>',
          'pip_deps': '',
          'kitsu_url': '', 'wiki_url': '', 'thumbnail': '',
          'env': '', 'site_env': '', 'user_env': '' },
        { 'name': 'Project 2', 'description': 'A second project', 'is_enabled': False, 'is_archived': False,
          'libreflow': '1.1.3',
          'redis_url': '://redis', 'redis_password': 'redis_password', 'redis_port': 6380, 'redis_cluster': '<cluster>', 'redis_db': 0,
          'mongo_url': '://mongo', 'mongo_user': 'mongo', 'mongo_password': 'mongo_password',
          'exchange_server_type': 'minio', 'exchange_server_url': '://exch_server', 'exchange_server_login': 'exch_server_login', 'exchange_server_password': 'exchange_server_password', 'exchange_server_bucket': '<bucket>',
          'pip_deps': '',
          'kitsu_url': '', 'wiki_url': '', 'thumbnail': '',
          'env': '', 'site_env': '', 'user_env': '' },
        { 'name': 'Project 3', 'description': 'A last project', 'is_enabled': True, 'is_archived': False,
          'libreflow': '1.0.0',
          'redis_url': '://redis', 'redis_password': 'redis_password', 'redis_port': 6380, 'redis_cluster': '<cluster>', 'redis_db': 0,
          'mongo_url': '://mongo', 'mongo_user': 'mongo', 'mongo_password': 'mongo_password',
          'exchange_server_type': 'minio', 'exchange_server_url': '://exch_server', 'exchange_server_login': 'exch_server_login', 'exchange_server_password': 'exchange_server_password', 'exchange_server_bucket': '<bucket>',
          'pip_deps': '',
          'kitsu_url': '', 'wiki_url': '', 'thumbnail': '',
          'env': '', 'site_env': '', 'user_env': '' },
    ])

    extensions = create_extensions([
        { 'name': 'extension_1', 'description': 'A basic extension', 'is_enabled': True, 'categories': 'tool' },
        { 'name': 'extension_2', 'description': 'Another extension', 'is_enabled': True, 'categories': 'blender,graphics' },
        { 'name': 'extension_3', 'description': 'A third extension', 'is_enabled': False, 'categories': '' },
        { 'name': 'extension_4', 'description': 'A fourth extension', 'is_enabled': False, 'categories': '' },
    ])
    for ext in extensions:
        disabled_ver = create_versions([
            { 'name': '<disabled>', 'description': '', 'is_enabled': True, 'service': '', 'url': '', 'path': '',
              'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
              'pypi': '', 'pip_deps': '', 'env': '' }
        ])[0]
        default_ver = create_versions([
            { 'name': '<default>', 'description': '', 'is_enabled': True, 'service': '', 'url': '', 'path': '',
              'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
              'pypi': '', 'pip_deps': '', 'env': '' }
        ])[0]
        link_version_to_extension(disabled_ver['uid'], ext['uid'])
        link_version_to_extension(default_ver['uid'], ext['uid'])
        set_version_as_ext_default(default_ver['uid'], ext['uid'])

    versions = create_versions([
        # latest versions
        { 'name': 'latest', 'description': 'Release version', 'is_enabled': True,
          'service': 'gitlab',
          'url': '://ext1-latest', 'path': '',
          'repo_group': '<lfs>', 'repo_project': '<ext1>', 'repo_ref_type': 'branch', 'repo_ref': 'master', 'repo_token': '<token>',
          'pypi': '', 'pip_deps': '', 'env': json.dumps([{ 'key': 'MY_VAR', 'value': 'default value' }, { 'key': 'ANOTHER_VAR', 'value': 'other' }]) },
        { 'name': 'latest', 'description': 'Release version', 'is_enabled': True,
          'service': 'gitlab',
          'url': '://ext2-latest', 'path': '',
          'repo_group': '<lfs>', 'repo_project': '<ext2>', 'repo_ref_type': 'branch', 'repo_ref': 'master', 'repo_token': '<token>',
          'pypi': '', 'pip_deps': '', 'env': '' },
        { 'name': 'latest', 'description': 'Release version', 'is_enabled': True,
          'service': 'gitlab',
          'url': '://ext3-latest', 'path': '',
          'repo_group': '<lfs>', 'repo_project': '<ext3>', 'repo_ref_type': 'branch', 'repo_ref': 'master', 'repo_token': '<token>',
          'pypi': '', 'pip_deps': '', 'env': json.dumps([{ 'key': 'EXAMPLE', 'value': 'example value' }]) },
        # alpha versions
        { 'name': 'alpha', 'description': 'Experimental alpha', 'is_enabled': True,
          'service': 'pypi',
          'url': '://ext1-alpha', 'path': '',
          'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
          'pypi': 'libreflow.extension.ext1==alpha', 'pip_deps': '',
          'env': json.dumps([{ 'key': 'ALPHA_VAR', 'value': 'an example value' }]) },
        { 'name': 'alpha', 'description': 'Experimental alpha', 'is_enabled': True,
          'service': 'pypi',
          'url': '://ext2-alpha', 'path': '',
          'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
          'pypi': 'libreflow.extension.ext2==alpha', 'pip_deps': '', 'env': '' },
        # beta versions
        { 'name': 'beta', 'description': 'Experimental beta', 'is_enabled': True,
          'service': 'pypi',
          'url': '://ext1-beta', 'path': '',
          'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
          'pypi': 'libreflow.extension.ext1==beta', 'pip_deps': '',
          'env': json.dumps([{ 'key': 'BETA_VAR_1', 'value': 'val1' }, { 'key': 'BETA_VAR_2', 'value': 'val2' }, { 'key': 'BETA_VAR_3', 'value': 'val3' }]) },
        { 'name': 'beta', 'description': 'Experimental beta', 'is_enabled': True,
          'service': 'github',
          'url': '://ext2-beta', 'path': '',
          'repo_group': '<lfs>', 'repo_project': '<ext2>', 'repo_ref_type': 'commit', 'repo_ref': 'cb0ce2f3ddfd5d3050c3f24066bc3e09cb0f18ce', 'repo_token': '<token>',
          'pypi': '', 'pip_deps': '', 'env': '' },
        { 'name': 'beta', 'description': 'Experimental beta', 'is_enabled': True,
          'service': 'github',
          'url': '://ext4-beta', 'path': '',
          'repo_group': '<lfs>', 'repo_project': '<ext4>', 'repo_ref_type': 'commit', 'repo_ref': 'fb0ce2f3ddfe5d3050c3f24066bc3e03cb0f18ce', 'repo_token': '<token>',
          'pypi': '', 'pip_deps': '', 'env': '' },
    ])

    ## User -> Site
    link_user_to_site(users[0]['uid'], sites[0]['uid'])
    link_user_to_site(users[0]['uid'], sites[1]['uid'])

    ## Site -> Project
    link_project_to_site(projects[0]['uid'], sites[0]['uid'])
    link_project_to_site(projects[1]['uid'], sites[0]['uid'])
    link_project_to_site(projects[2]['uid'], sites[0]['uid'])

    link_project_to_site(projects[0]['uid'], sites[1]['uid'])

    ## Extension -> Version
    # latest versions
    link_version_to_extension(versions[0]['uid'], extensions[0]['uid'])
    link_version_to_extension(versions[1]['uid'], extensions[1]['uid'])
    link_version_to_extension(versions[2]['uid'], extensions[2]['uid'])
    # alpha versions
    link_version_to_extension(versions[3]['uid'], extensions[0]['uid'])
    link_version_to_extension(versions[4]['uid'], extensions[1]['uid'])
    # beta versions
    link_version_to_extension(versions[5]['uid'], extensions[0]['uid'])
    link_version_to_extension(versions[6]['uid'], extensions[1]['uid'])
    link_version_to_extension(versions[7]['uid'], extensions[3]['uid'])

    ## Project -> Extension
    add_extension_to_project(versions[0]['uid'], projects[0]['uid'], 'project', env='')
    add_extension_to_project(versions[1]['uid'], projects[0]['uid'], 'project', env='')
    add_extension_to_project(versions[3]['uid'], projects[0]['uid'], 'site', site='LFS')
    add_extension_to_project(versions[6]['uid'], projects[0]['uid'], 'user', login='colibri')

    add_extension_to_project(versions[0]['uid'], projects[1]['uid'], 'project', env='')
    add_extension_to_project(versions[2]['uid'], projects[1]['uid'], 'project', env='')
    add_extension_to_project(versions[5]['uid'], projects[1]['uid'], 'user', login='colibri')
    add_extension_to_project(versions[4]['uid'], projects[1]['uid'], 'site', site='LFS')
    add_extension_to_project(versions[7]['uid'], projects[2]['uid'], 'user', login='colibri')
