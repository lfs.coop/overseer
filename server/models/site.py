# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from typing import List

from helpers.converters import PydanticToORM


class Site(BaseModel):
    code: str
    name: str = ''
    type: str = 'studio'
    is_enabled: bool = True
    is_archived: bool = False
    description: str = ''
    projects: List[str] = []


class SiteFull(Site):
    uid: str


def make_code(u: dict) -> str:
    return u.get('code', u['name'])


SiteInDB = PydanticToORM(
    SiteFull, 'Site',
    constructors = {
        'code': make_code,
    },
    relationships={
        'projects': {
            'target': 'models.project.ProjectInDB',
            'label': 'HAS'
        },
    })
