# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel

from helpers.converters import PydanticToORM


class Version(BaseModel):
    name: str
    description: str = ''
    is_enabled: bool = True
    service: str
    url: str = ''
    path: str = ''
    repo_project: str = ''
    repo_group: str = ''
    repo_ref_type: str = ''
    repo_ref: str = ''
    repo_token: str = ''
    pypi: str = ''
    pip_deps: str = ''
    env: str = ''


class VersionPayload(Version):
    extension_uid: str


class VersionFull(Version):
    uid: str
    extension: str


VersionInDB = PydanticToORM(
    VersionFull, 'Version',
    relationships={
        'extension': {
            'target': 'models.extension.ExtensionInDB',
            'label': 'FOR',
            'cardinality': 'One',
        },
    })
