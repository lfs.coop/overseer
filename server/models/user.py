# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from typing import List, Optional

from helpers.converters import PydanticToORM


class User(BaseModel):
    login: str
    name: str = ''
    is_enabled: bool = True
    is_archived: bool = False
    description: str = ''
    

class UserPayload(User):
    uid: Optional[str] = None


class UserFull(User):
    uid: str
    sites: List[str] = []
    projects: List[str] = []


def make_code(u: dict) -> str:
    return u.get('code', u['username'])


class RelSite(BaseModel):
    is_current: bool


UserInDB = PydanticToORM(
    UserFull, 'User',
    constructors = {
        'code': make_code,
    },
    relationships={
        'sites': {
            'target': 'models.site.SiteInDB',
            'label': 'IN',
            'model': RelSite,
        },
        'projects': {
            'target': 'models.project.ProjectInDB',
            'label': 'WORKS_ON',
        },
    })


class UserToken(BaseModel):
    access_token: str
    token_type: str


class UserTokenData(User):
    uid: str
    roles: List[str] = []
