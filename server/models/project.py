# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from typing import List

from helpers.converters import PydanticToORM
from security import hash_password

class ProjectBase(BaseModel):
    name: str
    description: str = ''
    is_enabled: bool = True
    is_archived: bool = False
    libreflow: str
    redis_url: str
    redis_password: str
    redis_port: int = 6380
    redis_cluster: str
    redis_db: int = 0
    mongo_url: str
    mongo_user: str
    mongo_password: str
    exchange_server_type: str = 'minio'
    exchange_server_url: str
    exchange_server_login: str
    exchange_server_password: str
    exchange_server_bucket: str
    pip_deps: str = ''
    kitsu_url: str = ''
    wiki_url: str = ''
    thumbnail: str = ''


class Project(ProjectBase):
    env: str = ''
    site_env: str = ''
    user_env: str = ''
    sites: List[str] = []
    project_extensions: List[str] = []
    site_extensions: List[str] = []
    user_extensions: List[str] = []


class ProjectFull(Project):
    uid: str


class RelProjectExtension(BaseModel):
    env: str
    is_default: bool

class RelSiteExtension(BaseModel):
    site: str

class RelUserExtension(BaseModel):
    login: str


def hash_redis_password(p: dict) -> str: return hash_password(p.get('redis_password', ''))
def hash_mongo_password(p: dict) -> str: return hash_password(p.get('mongo_password', ''))
def hash_exchange_server_password(p: dict) -> str: return hash_password(p.get('exchange_server_password', ''))


ProjectInDB = PydanticToORM(
    ProjectFull, 'Project',
    constructors={
        'redis_password': hash_redis_password,
        'mongo_password': hash_mongo_password,
        'exchange_server_password': hash_exchange_server_password,
    },
    relationships={
        'sites': {
            'target': 'models.site.SiteInDB',
            'label': 'HAS',
            'direction': 'from',
        },
        'project_extensions': {
            'target': 'models.version.VersionInDB',
            'label': 'PROJECT_USES',
            'model': RelProjectExtension,
        },
        'site_extensions': {
            'target': 'models.version.VersionInDB',
            'label': 'SITE_USES',
            'model': RelSiteExtension,
        },
        'user_extensions': {
            'target': 'models.version.VersionInDB',
            'label': 'USER_USES',
            'model': RelUserExtension,
        },
    })
