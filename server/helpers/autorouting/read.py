# -----------------------------------------------------------------------
# Copyright 2022 Mina Pêcheux

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from typing import Callable, Dict, List, Literal, Optional, Union


OrderingDirections = Union[Literal['asc'], Literal['desc']]


def list_instances_handler(db_schema: type) -> Callable:
    def _f(
        skip: int,
        limit: int,
        sort_by: Optional[str] = None,
        sort_dir: OrderingDirections = 'asc'
    ) -> List[Dict]:
        if hasattr(db_schema, 'before_read'):
            db_schema.before_read(multiple=True)
        if sort_by is not None:
            order_by = f'_.{sort_by}'
        else:
            order_by = None
        results = []
        for instance in db_schema.find_all(
                skip=skip,
                limit=limit,
                order_by=order_by):
            data = instance.to_json()
            if hasattr(db_schema, 'after_read'):
                data = instance.after_read(data)
            results.append(data)
        return results
    return _f


def get_instance_by_uid_handler(db_schema: type) -> Callable:
    def _f(uid: str) -> dict:
        if hasattr(db_schema, 'before_read'):
            db_schema.before_read(multiple=False)
        instance = db_schema.find_one(uid=uid)
        if instance:
            data = instance.to_json()
            if hasattr(db_schema, 'after_read'):
                data = instance.after_read(data)
            return data
        else:
            return None
    return _f
