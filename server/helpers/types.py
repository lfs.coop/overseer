# -----------------------------------------------------------------------
# Copyright 2022 Mina Pêcheux

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import typing
from pydantic import BaseModel


def is_union(t) -> bool:
    return typing.get_origin(t) is typing.Union


def is_nullable(t) -> bool:
    return type(None) in typing.get_args(t)


def get_real_type(t):
    orig = typing.get_origin(t)
    return orig if orig else t


def get_optional_real_type(t):
    x = list(typing.get_args(t))
    x.remove(type(None))
    return x[0]


def is_simple_collection(t):
    orig = typing.get_origin(t)
    if orig == list:
        for arg in typing.get_args(t):
            if issubclass(arg, BaseModel):
                return False
        return True
    return False
