# -----------------------------------------------------------------------
# Copyright 2024 Mina Pêcheux

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import os
from collections import deque

from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from helpers.utils import dict_to_neo4j


MODEL_RELATIONS = {}


def make_rel(id, type, dir, properties):
    r = f'-[{id}:{type} {dict_to_neo4j(properties)}]-'
    return r + '>' if dir == 'from' else '<' + r


def init():
    with open(os.path.join(os.getcwd(), 'data', 'relations.yaml'), 'r') as FILE:
        MODEL_RELATIONS.update(load(FILE, Loader=Loader))

    # solve reverse relations + format relations for Neo4j queries
    def read_rel_data(rel, dir):
        possible_types = []
        type_params = {}
        for i in range(len(rel)):
            if isinstance(rel[i], dict):
                k = list(rel[i].keys())
                possible_types.append(k[0])
                type_params[k[0]] = rel[i][k[0]]
            else:
                possible_types.append(rel[i])
                
        full_type = '|'.join(possible_types)
        return {
            'read': lambda x = 'r', only_type = -1, extra = {}: make_rel(
                x, full_type if only_type == -1 else possible_types[only_type], dir, extra),
            'create': lambda x = 'r', t = -1, extra = {}: make_rel(
                x, possible_types[t], dir, { **type_params.get(possible_types[t], {}), **extra }),
        }

    rel = {}
    for source in MODEL_RELATIONS:
        if source not in rel:
            rel[source] = {}
        for target in MODEL_RELATIONS[source]:
            rel[source][target] = read_rel_data(MODEL_RELATIONS[source][target], dir='from')
            if not target in rel:
                rel[target] = {}
            rel[target][source] = read_rel_data(MODEL_RELATIONS[source][target], dir='to')

    MODEL_RELATIONS.update(rel)


def rel_cypher(source, target, action, # 'action' can be: read, create, delete
               source_condition = '', target_condition = '',
               intermediate = {}, extra = {}):

    if not source in MODEL_RELATIONS: # entity type isn't registered in relations
        return None

    if action == 'delete': # (this is only used for multi-step paths)
        path = deque()
        visited = set()
        _rel_dfs(target, source, path, visited)
        if len(path) == 0:
            return None

        results = {}
        for target_uid in intermediate:
            p = path.copy()
            rels = ['(s)']

            i = 0
            current_entity = source
            middle_entity = p.pop()
            x = intermediate[target_uid].get(middle_entity, None)
            r = MODEL_RELATIONS[current_entity][middle_entity]['read'](f'r{i}', 0)
            rels.append(r)
            rels.append(f'(x{i}:{middle_entity} {{uid: "{x["uid"]}"}})')
            while x and not x['link'] and len(path) > 0:
                current_entity = middle_entity
                middle_entity = p.pop()
                x = intermediate[target_uid].get(middle_entity, None)
                r = MODEL_RELATIONS[current_entity][middle_entity]['read'](f'r{i}', 0)
                rels.append(r)
                rels.append(f'(x{i}:{middle_entity}) {{uid: "{x["uid"]}"}})')
            results[target_uid] = { 'rel': ''.join(rels), 'to_del': f'r{i}' }
        return results

    s = f'(s)' if action != 'read' else f'(s:{source} {source_condition})'
    t = f'(t)' if action != 'read' else f'(t:{target} {target_condition})'

    if target in MODEL_RELATIONS[source]: # simple case: directly registered path
        rel = MODEL_RELATIONS[source][target]
        return f'{s}{rel["read"]()}{t}'
    else: # perhaps we can get a valid multiple steps path (using a DFS search)
    
        path = deque()
        visited = set()
        _rel_dfs(target, source, path, visited)

        # if 'intermediate' is not empty, then we need to check for specific intermediate
        # entities on the path, so we can't batch the process anymore and check for the
        # target uids globally - instead, we need to do each match one by one
        #
        # e.g. Project > Extension: if we need to match intermediate Versions specifically,
        #   then we have to treat each Extension separately as well
        batch = len(intermediate) == 0

        # if only one process, return a single relation cypher chunk
        if batch:
            return s + _rel_process_dfs(path, source, action=action, extra=extra) + t
        # else, return one relation per unique path

        res = {}
        for target_uid in intermediate:
            res[target_uid] = _rel_process_dfs(
                path.copy(), source,
                intermediate=intermediate[target_uid],
                action=action, extra=extra)
        return res


def _rel_dfs(start, end, path, visited):
    if start == end:
        return True
    path.append(start)
    visited.add(start)
    for c in MODEL_RELATIONS[start]:
        if not c in visited:
            visited.add(c)
            if _rel_dfs(c, end, path, visited):
                return True
            path.pop()


def _rel_process_dfs(path, source, action, intermediate = None, extra = {}):
    matches = []
    rels = []
    current_entity = source
    current_match = '(s)'
    i = 0
    while len(path) > 0:
        middle_entity = path.pop()
        if not intermediate:
            r = MODEL_RELATIONS[current_entity][middle_entity][action](f'r{i}', 0, {})
            rels.append(r)
            # ignore for last step because target is written with
            # optional condition outside the loop
            if len(path) > 0:
                rels.append(f'(:{middle_entity})')
        else:
            if middle_entity in intermediate:
                e = extra.get(intermediate[middle_entity]['uid'], {})
                r = MODEL_RELATIONS[current_entity][middle_entity][action](f'r{i}', 0, e)
                matches.append(f'(x{i}:{middle_entity} {{uid: "{intermediate[middle_entity]["uid"]}"}})')
                if intermediate[middle_entity]["link"]:
                    rels.append(current_match + r + f'(x{i})')
                    current_match = f'(x{i})'

        if len(path) > 0:
            current_entity = middle_entity
            i += 1

    if not intermediate:
        return ''.join(rels)
    return { 'matches': ','.join(matches), 'relations': ','.join(rels) }
