from keycloak import KeycloakAdmin, KeycloakOpenID
from config import (
    KEYCLOAK_SERVER_URL,
    KEYCLOAK_ADMIN_USERNAME,
    KEYCLOAK_ADMIN_PASSWORD,
    KEYCLOAK_REALM_NAME,
    KEYCLOAK_CLIENT_ID,
    KEYCLOAK_CLIENT_NAME,
    KEYCLOAK_CLIENT_SECRET_KEY,
)

def get_admin_client() -> KeycloakAdmin:
    return KeycloakAdmin(
        server_url=KEYCLOAK_SERVER_URL,
        username=KEYCLOAK_ADMIN_USERNAME,
        password=KEYCLOAK_ADMIN_PASSWORD,
        realm_name=KEYCLOAK_REALM_NAME,
        client_id=KEYCLOAK_CLIENT_NAME,
        client_secret_key=KEYCLOAK_CLIENT_SECRET_KEY,
        verify=True)

def get_openid_client() -> KeycloakOpenID:
    return KeycloakOpenID(
        server_url=KEYCLOAK_SERVER_URL,
        realm_name=KEYCLOAK_REALM_NAME,
        client_id=KEYCLOAK_CLIENT_NAME,
        client_secret_key=KEYCLOAK_CLIENT_SECRET_KEY)

def get_user_roles(user_id: str) -> list:
    roles = get_admin_client().get_client_roles_of_user(
        user_id=user_id,
        client_id=KEYCLOAK_CLIENT_ID)
    roles = [r['name'] for r in roles]
    return roles

def authenticate_user(username: str, password: str) -> dict:
    try:
        client = get_openid_client()
        token = client.token(username, password)
        token_info = client.introspect(token['access_token'])
        return {
            'uid': token_info['sub'],
            'username': token_info['preferred_username'],
            'firstname': token_info.get('given_name', ''),
            'lastname': token_info.get('family_name', ''),
            'email': token_info.get('email', ''),
            'roles': get_user_roles(token_info['sub']),
        }
    except:
        return None
