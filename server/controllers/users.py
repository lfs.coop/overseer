# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import json
from keycloak_client import get_admin_client
from database import session_scope, db
from models.project import ProjectInDB
from models.site import SiteInDB
from models.user import UserInDB
from helpers.tools import connect, get_connection_prop_name
from helpers.utils import PRIMARY_KEY, create_uid, invalid_input_data_exception



def get_user_by_uid(user_uid: str) -> dict:
    user = UserInDB.find_one(uid=user_uid)
    if user:
        return user.to_json()


def get_user(login: str) -> dict:
    user = UserInDB.find_one(login=login)
    if user:
        return user.to_json()
    else:
        return None


def get_users_metalist() -> list:
    results = []
    for instance in UserInDB.find_all():
        results.append(instance.to_json(only_meta=True))
    return results


def create_user(data: dict) -> dict:
    data[PRIMARY_KEY] = create_uid()
    for k, v in getattr(UserInDB, '_constructors').items():
        data[k] = v(data)
    with session_scope() as session:
        instance = UserInDB(**data)
        session.create(instance)
        return instance.to_json()

def link_user_to_site(user_login: str, site: str) -> dict:
    user = UserInDB.find_one(login=user_login)
    if not user:
        print('Could not find user "%s"' % user_login)
        raise invalid_input_data_exception
    
    site = SiteInDB.find_one(uid=site)
    if not site:
        print('Could not find site "%s"' % site)
        raise invalid_input_data_exception

    existing_sites = getattr(user, get_connection_prop_name('sites', SiteInDB))
    has_current_site = any(s.is_enabled for s in existing_sites)
    connect(user, site, 'sites', { 'is_current': not has_current_site })

    with session_scope() as session:
        session.push(user)
        
    data = user.to_json()
    data['sites'] = [s['item'] for s in data['sites']]
    return data

def register_keycloak_users() -> list:
    keycloak_users = get_admin_client().get_users({})

    all_projects = ProjectInDB.find_all()
    with session_scope() as session:
        for k_user in keycloak_users:
            username = k_user['username']
            already_exists = UserInDB.find_one(login=username)
            if not already_exists:
                u = UserInDB(
                    uid = create_uid(),
                    login = username,
                    name = username,
                    is_enabled = True,
                    description = '',
                )
                session.create(u)
        for db_user in UserInDB.find_all():
            if not any(x['username'] == db_user.login for x in keycloak_users):
                n = db_user.login
                # delete all project overrides that use this user
                # - in extension version
                db.run(f'MATCH (:Project)-[r:USER_USES {{login: "{n}"}}]->(:Version) DELETE r')
                # - in env variables
                for p in all_projects:
                    env = json.loads(p.user_env if hasattr(p, 'user_env') and p.user_env != '' else '[]')
                    if n in env:
                        del env[n]
                    p.user_env = json.dumps(env)
                    session.push(p)

                # delete the user entity in the DB
                session.delete(db_user)

    return [u.to_json() for u in UserInDB.find_all()]
