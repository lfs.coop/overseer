# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from database import db
from models.user import UserInDB

def get_users(site_code: str):
    users = db.query(f'MATCH (u:User)-[IN]->(s:Site {{code: "{site_code}"}}) RETURN u').data()
    data = []
    for u in users:
        data.append(UserInDB.find_one(uid=u['u']['uid']).to_json())
    return data

def has_user(site_code: str, user_login: str):
    check = db.query(f'''MATCH (u:User {{login: "{user_login}"}}), (s:Site {{code: "{site_code}"}})
                     RETURN EXISTS((u)-[:IN]->(s)) as r''').data()
    return check[0]['r']
