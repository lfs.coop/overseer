# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from database import session_scope
from models.extension import ExtensionInDB
from models.version import VersionInDB
from helpers.tools import connect, get_connection_prop_name
from helpers.utils import create_uid, invalid_input_data_exception, name_already_taken_exception

def create_extension(data: dict) -> dict:
    used_names = [e.name for e in ExtensionInDB.find_all()]
    if data['name'] in used_names:
        raise name_already_taken_exception

    # create extension
    data['uid'] = create_uid()
    extension = ExtensionInDB(**data)

    # create base version instances
    base_versions_data = [
        { 'name': '<disabled>', 'description': '', 'is_enabled': True, 'service': '', 'url': '', 'path': '',
          'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
          'pypi': '', 'pip_deps': '', 'env': '' },
        { 'name': '<default>', 'description': '', 'is_enabled': True, 'service': '', 'url': '', 'path': '',
          'repo_group': '', 'repo_project': '', 'repo_ref_type': '', 'repo_ref': '', 'repo_token': '',
          'pypi': '', 'pip_deps': '', 'env': '' },
    ]
    versions = []
    for version_data in base_versions_data:
        version_data['uid'] = create_uid()
        versions.append(VersionInDB(**version_data))

    with session_scope() as session:
        session.push(extension)
        for version in versions:
            session.push(version)
        
    # auto link extension to base versions
    for version in versions:
        connect(extension, version, 'versions')

        if version.name == '<default>':
            connect(extension, version, 'default_version')

    with session_scope() as session:
        session.push(extension)

    return {
        'extension': extension.to_json(),
        'versions': [v.to_json() for v in versions]
    }

def delete_extension(extension_uid: str) -> None:
    extension = ExtensionInDB.find_one(uid=extension_uid)
    if not extension:
        print('Could not find extension "%s"' % extension_uid)
        raise invalid_input_data_exception

    versions = getattr(extension, get_connection_prop_name('versions', VersionInDB))
    with session_scope() as session:
        for version in versions:
            session.delete(version)
        session.delete(extension)

def delete_all_extensions() -> None:
    for extension in ExtensionInDB.find_all():
        delete_extension(extension.uid)
