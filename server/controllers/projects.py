# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import json
from database import db, session_scope
from models.extension import ExtensionInDB
from models.project import ProjectInDB
from models.site import SiteInDB
from models.user import UserInDB
from models.version import VersionInDB
from helpers.tools import connect, get_connection_prop_name
from helpers.utils import create_uid, dict_to_neo4j, invalid_input_data_exception, name_already_taken_exception

def update_project_extension_env(project_uid: str, data: dict) -> dict:
    for version_uid, env in data.items():
        e = json.dumps(env)
        db.run(f'MATCH (p:Project {{uid: "{project_uid}"}})-[r:PROJECT_USES]->(v:Version {{uid: "{version_uid}"}}) SET r.env=\'{e}\'')

def update_project_extension_overrides(project_uid: str, versions_uid: list) -> dict:
    for override in ['site', 'user']:
        rel_type = override.upper() + '_USES'

        # remove current
        db.run(f'MATCH (p:Project {{uid: "{project_uid}"}})-[r:{rel_type}]->(v:Version) DELETE r')

        # add new
        for item in versions_uid[override]:
            version_uid = item['version']
            rel_data = {}
            if override == 'site':
                rel_data['site'] = item['site']
            elif override == 'user':
                rel_data['login'] = item['user']
            db.run(f'''MATCH (p:Project {{uid: "{project_uid}"}}), (v:Version {{uid: "{version_uid}"}})
                   CREATE (p)-[:{rel_type} {dict_to_neo4j(rel_data)}]->(v)''')
            
    return ProjectInDB.find_one(uid=project_uid).to_json()

def get_resolved_project(project_name: str, site_code: str, user_login: str):
    force_full = True # return full JSON of entities, not just UIDs (regardless of server config)

    project = ProjectInDB.find_one(name=project_name)
    site = SiteInDB.find_one(code=site_code)
    user = UserInDB.find_one(login=user_login)
    all_extensions = [ext.to_json(force_full=force_full) for ext in ExtensionInDB.find_all()]
    if not project or not site or not user or not all_extensions or len(all_extensions) == 0:
        return None

    data = project.to_json(force_full=force_full)

    # compute resolved env
    project_env = json.loads(data['env'] if data['env'] != '' else '[]')
    site_env = json.loads(data['site_env'] if data['site_env'] != '' else '{}')
    user_env = json.loads(data['user_env'] if data['user_env'] != '' else '{}')
    env = {}
    for item in project_env: env[item['key']] = item['value']
    for item in site_env.get(site.uid, []): env[item['key']] = item['value']
    for item in user_env.get(user_login, []): env[item['key']] = item['value']
    data['env'] = env
    del data['site_env']
    del data['user_env']

    extensions = []
    project_ext = getattr(project, get_connection_prop_name('project_extensions', VersionInDB))

    for proj_version in project_ext:
        # Use default version of extension
        version = proj_version
        version_json = version.to_json(force_full=force_full)

        # Check for site overrides
        site_extensions = {}
        for site_ext in data['site_extensions']:
            if isinstance(site_ext['item'], str):
                ext_uid = site_ext['item']
            elif isinstance(site_ext['item'], dict):
                ext_uid = site_ext['item']['uid']
            
            if ext_uid not in site_extensions:
                site_extensions[ext_uid] = {}
                site_extensions[ext_uid]['props'] = dict(site=[])
            
            if site_ext['rel']['label'] == 'SITE_USES':
                site_extensions[ext_uid]['props']['site'].append(site_ext['rel']['site'])

        for version_uid, version_data in site_extensions.items():
            site_version = VersionInDB.find_one(uid=version_uid)
            site_version_json = site_version.to_json(force_full=force_full)

            # Use site override version
            if site_code in version_data['props']['site'] and site_version_json['extension']['item'] == version_json['extension']['item']:
                version = site_version
                break

        # Check for user overrides
        user_extensions = {}
        for user_ext in data['user_extensions']:
            if isinstance(user_ext['item'], str):
                ext_uid = user_ext['item']
            elif isinstance(user_ext['item'], dict):
                ext_uid = user_ext['item']['uid']
            
            if ext_uid not in user_extensions:
                user_extensions[ext_uid] = {}
                user_extensions[ext_uid]['props'] = dict(login=[])
            
            if user_ext['rel']['label'] == 'USER_USES':
                user_extensions[ext_uid]['props']['login'].append(user_ext['rel']['login'])

        for version_uid, version_data in user_extensions.items():
            user_version = VersionInDB.find_one(uid=version_uid)
            user_version_json = user_version.to_json(force_full=force_full)

            # Use user override version
            if user_login in version_data['props']['login'] and user_version_json['extension']['item'] == version_json['extension']['item']:
                version = user_version
                break
        
        disabled = version.name == '<disabled>'

        if not disabled:
            ext = None
            for e in all_extensions:
                if any(
                    isinstance(v['item'], dict) and v['item']['uid'] == version.uid or \
                    isinstance(v['item'], str) and v['item'] == version.uid
                    for v in e.get('versions', [])):
                        ext = e
                        break
            
            if ext:
                if ext['is_enabled']:
                    del ext['is_enabled']
                    del ext['versions']
                    extensions.append({
                        'uid': ext['uid'],
                        'name': ext['name'],
                        'description': ext['description'],
                        'categories': ext['categories'].split(','),
                        'version': version.to_json(force_full=force_full),
                    })

    data['extensions'] = extensions
    del data['project_extensions']
    del data['site_extensions']
    del data['user_extensions']

    return data

def create_project(site: (str, None), data: dict) -> dict:
    if site:
        site = SiteInDB.find_one(uid=site)
        if not site:
            print('Could not find site "%s"' % site)
            raise invalid_input_data_exception

    used_names = [p.name for p in ProjectInDB.find_all()]
    if data['name'] in used_names:
        raise name_already_taken_exception

    # create project instance
    data['uid'] = create_uid()
    project = ProjectInDB(**data)
    with session_scope() as session:
        session.push(project)
        
    # auto link to given site
    if site:
        connect(site, project, 'projects')
        with session_scope() as session:
            session.push(site)

    return project.to_json()

def get_users(project_name: str):
    project_users = db.query(f'MATCH (u:User)-[WORKS_ON]->(p:Project {{name: "{project_name}"}}) RETURN u').data()
    data = []
    for u in project_users:
        data.append(UserInDB.find_one(uid=u['u']['uid']).to_json())
    return data

def has_user(project_name: str, user_login: str):
    check = db.query(f'''MATCH (u:User {{login: "{user_login}"}}), (p:Project {{name: "{project_name}"}})
                     RETURN EXISTS((u)-[:WORKS_ON]->(p)) as r''').data()
    return len(check) and check[0]['r']

def has_site(project_name: str, site_code: str):
    check = db.query(f'''MATCH (s:Site {{code: "{site_code}"}}), (p:Project {{name: "{project_name}"}})
                     RETURN EXISTS((s)-[:HAS]->(p)) as r''').data()
    return len(check) and check[0]['r']
