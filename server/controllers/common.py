# -----------------------------------------------------------------------
# Copyright 2024 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from collections import defaultdict
from py2neo import IN

from database import db
from models.extension import ExtensionInDB
from models.project import ProjectInDB
from models.site import SiteInDB
from models.user import UserInDB
from models.version import VersionInDB

from relations import rel_cypher
from helpers.utils import create_uid, PRIMARY_KEY
from helpers.autorouting import HANDLERS


def get_entity_neighbors(type: str, uid: str, searched_types: list) -> dict:
    model = globals().get(f'{type}InDB')
    instance = model.find_one(uid=uid)

    relations = {}
    for t in searched_types:
        c = rel_cypher(type, t, 'read', source_condition=f'{{uid: "{uid}"}}')
        data = db.run(f'MATCH {c} RETURN DISTINCT t').data()
        relations[t] = [d['t'] for d in data]

    data = {
        'type': type,
        'value': uid,
        'text': instance.name,
        'relations': relations
    }
    return data


def link(data: dict):
    intermediate = data['intermediate_uids']
    rel = rel_cypher(data['source_type'], data['target_type'],
                     'create', intermediate=intermediate, extra=data['extra'])
    
    # if no specific intermediate entities, we can batch process and
    # check for target uids globally
    if len(intermediate) == 0:
        queries = ['''MATCH (s:{0} {{uid: "{1}"}}), (t:{2})
            WHERE t.uid IN {3} CREATE {4}'''.format(
                data['source_type'], data['source_uid'],
                data['target_type'], data['target_uids'],
                rel
            )]
    # else, we need to process each one separately
    else:
        queries = ['''MATCH (s:{0} {{uid: "{1}"}}),
            {2},
            (t:{3} {{uid: "{4}"}})
            CREATE {5}'''.format(
                data['source_type'], data['source_uid'],
                rel[target_uid]['matches'],
                data['target_type'], target_uid,
                rel[target_uid]['relations']
            ) for target_uid in intermediate]

    for query in queries:
        db.run(query)


def unlink(data: dict):
    intermediate = data['intermediate_uids']
    
    # if no specific intermediate entities, we can batch process and
    # check for target uids globally
    if len(intermediate) == 0:
        queries = ['''MATCH (:{0} {{uid: "{1}"}})-[r]-(t:{2})
            WHERE t.uid IN {3} DELETE r'''.format(
                data['source_type'], data['source_uid'],
                data['target_type'], data['target_uids'],
            )]
    # else, we need to process each one separately
    else:
        rel = rel_cypher(data['source_type'], data['target_type'],
                        'delete', intermediate=intermediate)
        queries = ['MATCH (s:{0} {{uid: "{1}"}}), {2} DELETE {3}'.format(
                data['source_type'], data['source_uid'],
                rel[target_uid]['rel'],
                rel[target_uid]['to_del']
            ) for target_uid in intermediate]

    for query in queries:
        db.run(query)


def relink(data: dict):
    rel = f'-[r:{data["link_type"]}]-'
    if data['link_dir'] == 'from':
        rel = rel + '>'
    else:
        rel = '<' + rel

    # unlink old
    p = db.query('''MATCH (s:{0} {{uid: "{1}"}}){2}(t:{3}) WHERE t.uid IN {4}
           WITH r, [k in keys(r) WHERE NOT(r[k] is null) | [k, r[k]]] as params
           DELETE r
           RETURN params'''.format(
        data['source_type'], data['source_uid'], rel, data['target_type'], data['target_old_uids'])).data()[0]['params']
    p = { k: v for k, v in p }
    if len(data['extra']) > 0:
        p.update(data['extra'])
    p = '{' + ','.join(f'{k}: ' + str((v, "'%s'" % v)[isinstance(v, str)]) for k, v in p.items()) + '}'
    # link new
    db.run('MATCH (s:{0} {{uid: "{1}"}}), (t:{2}) WHERE t.uid IN {3} CREATE (s){4}(t) SET r={5}'.format(
        data['source_type'], data['source_uid'], data['target_type'], data['target_new_uids'], rel, p))

    if data['options'].get('reconnectProjects', False):
        db.run('''MATCH (p:Project)-[r:PROJECT_USES {{is_default: true}}]->(v:Version)-[:FOR]->(e:Extension {{uid: "{0}"}}),
               (v2:Version {{uid: "{1}"}})
               CREATE (p)-[r2:PROJECT_USES {{is_default: true}}]->(v2) SET r2=r
               DELETE r
               '''.format(
            data['source_uid'],  data['target_new_uids'][0]))


def duplicate(source_type: str, data: dict):
    source_uid = data[PRIMARY_KEY]
    target_uid = create_uid()

    creator = HANDLERS[f'{source_type}__create_instance_handler']
    model = globals().get(f'{source_type}InDB')

    data[PRIMARY_KEY] = target_uid

    links_already_handled = False

    # for versions, pre-search the uid of the extension to handle it
    # during the creation
    if source_type == 'Version':
        data['extension_uid'] = db.query(f'''MATCH (s:Version {{uid: "{source_uid}"}})-[:FOR]->(t)
                                         RETURN DISTINCT t.uid as target''').data()[0]['target']
        links_already_handled = True

    # for extensions, start by copying all versions as new entities, and
    # store the new uids in the creation data to auto-link
    elif source_type == 'Extension':
        dft_version_name = db.query(f'''MATCH (v:Version)<-[:HAS_DEFAULT]-(e:Extension {{uid: "{source_uid}"}})
                                   RETURN v.name as v''').data()[0]['v']
        old_versions = db.query(f'''MATCH (v:Version)-[:FOR]->(e:Extension {{uid: "{source_uid}"}})
                                RETURN COLLECT(v) as versions''').data()[0]['versions']
        new_version_uids = [(v['uid'], create_uid()) for v in old_versions]
        new_default_version_uid = ''
        for old_uid, new_uid in new_version_uids:
            v = db.query(f'''MATCH (v:Version {{uid: "{old_uid}"}}) CREATE (new:Version)
                   SET new = v SET new.uid = "{new_uid}" RETURN new''').data()[0]['new']
            if v['name'] == dft_version_name:
                new_default_version_uid = v['uid']
        new_version_uids = [x[1] for x in new_version_uids]
        data['versions_uid'] = new_version_uids
        data['default_version_uid'] = new_default_version_uid
        creator(data)

        return {
            'extension': model.find_one(uid=target_uid).to_json(),
            'versions': [v.to_json() for v in VersionInDB.find_all(uid=IN(new_version_uids))]
        }

    creator(data)

    # for the rest, re-duplicate links to other entities (without copying these neighbor entities)
    if not links_already_handled:
        out_relations = defaultdict(list)
        in_relations = defaultdict(list)
        for r in db.query(f'''MATCH (s:{source_type} {{uid: "{source_uid}"}})
            -[r]->(t) RETURN DISTINCT type(r) as type,
            [k in keys(r) WHERE NOT(r[k] is null) | [k, r[k]]] as params,
            t.uid as target''').data():
            out_relations[r['type']].append(r)
        for r in db.query(f'''MATCH (s:{source_type} {{uid: "{source_uid}"}})
            <-[r]-(t) RETURN DISTINCT type(r) as type,
            [k in keys(r) WHERE NOT(r[k] is null) | [k, r[k]]] as params,
            t.uid as target''').data():
            in_relations[r['type']].append(r)

        for rel_type in out_relations:
            rel = out_relations[rel_type]
            if not isinstance(rel, list):
                rel = [rel]
            db.run('''UNWIND [{0}] as row MATCH (s {{uid: "{1}"}}), (t {{uid: row.target}})
                CREATE (s)-[r:{2}]->(t) SET r += row.params'''.format(
                    ','.join(
                        '{' + f'target: "{r["target"]}",type: "{r["type"]}",' +
                        'params:{' + ','.join(f'{k}: ' + str((v, "'%s'" % v)[isinstance(v, str)])
                                for k,v in r['params']) + '}' + '}'
                        for r in rel),
                    target_uid, rel_type
                ))
        for rel_type in in_relations:
            rel = in_relations[rel_type]
            if not isinstance(rel, list):
                rel = [rel]
            db.run('''UNWIND [{0}] as row MATCH (s {{uid: "{1}"}}), (t {{uid: row.target}})
                CREATE (s)<-[r:{2}]-(t) SET r += row.params'''.format(
                    ','.join(
                        '{' + f'target: "{r["target"]}",type: "{r["type"]}",' +
                        'params:{' + ','.join(f'{k}: ' + str((v, "'%s'" % v)[isinstance(v, str)])
                                for k,v in r['params']) + '}' + '}'
                        for r in rel),
                    target_uid, rel_type
                ))

    return model.find_one(uid=target_uid).to_json()
