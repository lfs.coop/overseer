# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

import os
from dotenv import load_dotenv

load_dotenv('../.env')

APP_VERSION = '1.0.2'
API_PREFIX = os.getenv('API_PREFIX', '')

# global config
# . auth
# (generated with: openssl rand -hex 32)
TOKEN_SECRET_KEY = os.getenv('TOKEN_SECRET_KEY')
TOKEN_ALGORITHM = 'HS256'
TOKEN_EXPIRE_MINUTES = 7 * 24 * 60  # one week token

# server config
ORIGINS_WHITELIST = []

# keycloak config
KEYCLOAK_SERVER_URL = os.path.join(os.getenv('KEYCLOAK_HOST'), 'auth').rstrip('/') + '/'
KEYCLOAK_ADMIN_USERNAME = os.getenv('KEYCLOAK_ADMIN_USERNAME')
KEYCLOAK_ADMIN_PASSWORD = os.getenv('KEYCLOAK_ADMIN_PASSWORD')
KEYCLOAK_REALM_NAME = os.getenv('KEYCLOAK_REALM')
KEYCLOAK_CLIENT_ID = os.getenv('KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_NAME = os.getenv('KEYCLOAK_CLIENT_NAME')
KEYCLOAK_CLIENT_SECRET_KEY = os.getenv('KEYCLOAK_CLIENT_SECRET')
