# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from typing import Dict, Any, List, Optional

from fastapi import Depends
from security import oauth2_scheme
from tokens import check_token

from models.project import Project, ProjectBase, ProjectFull
import controllers.projects as pr

class ResolvedProject(ProjectBase):
    env: Dict[str, Any]

class ProjectPayload(BaseModel):
    site: Optional[str]
    data: Project


def create_router(app):
    
    # not generated by autorouter, to get custom behaviour with auto-linking
    @app.post('/projects', tags=['projects'], response_model=ProjectFull)
    async def create_project(payload: ProjectPayload, token: str = Depends(oauth2_scheme)) -> ProjectFull:
        check_token(token, ['admin'])
        return pr.create_project(**payload.dict())
    
    @app.patch('/project-extension-env/{uid}', tags=['projects'], response_model=Any)
    async def update_project_extension_env(data: dict, uid: str, token: str = Depends(oauth2_scheme)) -> Project:
        check_token(token, ['admin'])
        return pr.update_project_extension_env(uid, data)
    
    @app.patch('/project-extension-overrides/{uid}', tags=['projects'], response_model=Any)
    async def update_project_extension_overrides(versions_uid: Dict[str, Any], uid: str, token: str = Depends(oauth2_scheme)) -> Project:
        check_token(token, ['admin'])
        return pr.update_project_extension_overrides(uid, versions_uid)

    @app.get('/project-resolve', tags=['projects'], response_model=Any)
    async def get_resolved_project(project: str, site: str, user: str, token: str = Depends(oauth2_scheme)) -> Project:
        check_token(token)
        return pr.get_resolved_project(project, site, user)

    @app.get('/project-users', tags=['projects'], response_model=List[Any])
    async def get_project_users(project: str, token: str = Depends(oauth2_scheme)) -> List[Any]:
        check_token(token)
        return pr.get_users(project)

    @app.get('/project-check-has-user', tags=['projects'], response_model=bool)
    async def check_project_has_user(project: str, user: str, token: str = Depends(oauth2_scheme)) -> bool:
        check_token(token)
        return pr.has_user(project, user)

    @app.get('/project-check-has-site', tags=['projects'], response_model=bool)
    async def check_project_has_site(project: str, site: str, token: str = Depends(oauth2_scheme)) -> bool:
        check_token(token)
        return pr.has_site(project, site)
