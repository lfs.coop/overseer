# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from fastapi import Depends
from fastapi.security import OAuth2PasswordRequestForm
from jose import JWTError
from typing import List

from keycloak_client import authenticate_user
from models.user import User, UserFull, UserToken, UserTokenData, UserInDB
from controllers.users import get_user_by_uid, get_user, get_users_metalist, link_user_to_site, register_keycloak_users
from tokens import check_token, decode_token, get_access_token
from security import (credentials_exception,
                      invalid_auth_exception,
                      oauth2_scheme)

class UserLinkPayload(BaseModel):
    user_login: str
    site: str


async def get_current_user(token: str = Depends(oauth2_scheme)) -> UserFull:
    try:
        payload = decode_token(token)
        if payload.get('username') is None or payload.get('uid') is None:
            raise credentials_exception
        user_data = get_user(payload['username'])
        if user_data:
            payload.update(user_data)
        else:
            payload.update({'login': '', 'code': '', 'sites': []})
        token_data = UserTokenData(**payload)
    except JWTError:
        raise credentials_exception
    return token_data


def create_router(app):

    @app.get('/users-me', tags=['users'], response_model=UserTokenData)
    async def read_users_me(current_user: UserFull = Depends(get_current_user)) -> UserTokenData:
        return current_user

    @app.get('/users-meta', tags=['users'], response_model=List[User])
    async def get_users_meta(current_user: UserFull = Depends(get_current_user)) -> List[User]:
        return get_users_metalist()

    @app.patch('/users-link', tags=['users'], response_model=UserFull)
    async def update_user_link_to_site(link_payload: UserLinkPayload, token: str = Depends(oauth2_scheme)) -> UserFull:
        check_token(token, ['admin'])
        return link_user_to_site(**link_payload.dict())

    @app.post('/token', response_model=UserToken)
    async def token(form_data: OAuth2PasswordRequestForm = Depends()) -> UserToken:
        if not UserInDB.find_one(login=form_data.username):
            raise invalid_auth_exception

        user = authenticate_user(form_data.username, form_data.password)
        if not user:
            raise invalid_auth_exception
        access_token = get_access_token(user)
        return {'access_token': access_token, 'token_type': 'bearer'}

    @app.patch('/token', response_model=UserToken)
    async def update_token(token: str = Depends(oauth2_scheme)) -> UserToken:
        user = check_token(token)
        updated_user = get_user_by_uid(user)
        access_token = get_access_token(updated_user)
        return {'access_token': access_token, 'token_type': 'bearer'}

    @app.patch('/register-keycloak-users', response_model=List[UserInDB.response_model])
    async def update_register_keycloak_users(token: str = Depends(oauth2_scheme)) -> List[UserInDB.response_model]:
        return register_keycloak_users()
