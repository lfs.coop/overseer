# -----------------------------------------------------------------------
# Copyright 2023 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from typing import Any, List

from fastapi import Depends
from security import oauth2_scheme
from tokens import check_token

import controllers.sites as s


def create_router(app):

    @app.get('/site-users', tags=['sites'], response_model=List[Any])
    async def get_site_users(site: str, token: str = Depends(oauth2_scheme)) -> List[Any]:
        check_token(token)
        return s.get_users(site)

    @app.get('/site-check-has-user', tags=['sites'], response_model=bool)
    async def check_site_has_user(site: str, user: str, token: str = Depends(oauth2_scheme)) -> bool:
        check_token(token)
        return s.has_user(site, user)
