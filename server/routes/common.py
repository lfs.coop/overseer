# -----------------------------------------------------------------------
# Copyright 2024 M. Pêcheux, Les Fées Spéciales

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at the root of the repo.

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------

from pydantic import BaseModel
from typing import Any, Dict, List

from fastapi import Depends
from security import oauth2_scheme
from tokens import check_token

import controllers.common as com


class EntityNeighbors(BaseModel):
    type: str
    text: str
    value: str
    relations: Dict

class LinkQueryData(BaseModel):
    source_type: str
    source_uid: str
    target_type: str
    target_uids: List[str]
    intermediate_uids: Dict[str, Dict[str, Any]] = {}
    extra: dict = {}

class RelinkQueryData(BaseModel):
    source_type: str
    source_uid: str
    target_type: str
    link_type: str
    link_dir: str
    target_old_uids: List[str]
    target_new_uids: List[str]
    extra: dict = {}
    options: dict = {} # special options for some cascading relinks (e.g. default version > project that use defaults)


def create_router(app):
    
    @app.patch('/entity-neighbors/{type}/{uid}', response_model=EntityNeighbors)
    async def get_entity_neighbors(type: str, uid: str, searched_types: list, token: str = Depends(oauth2_scheme)) -> EntityNeighbors:
        check_token(token)
        return com.get_entity_neighbors(type, uid, searched_types)
    
    @app.patch('/entity-link-to-multiple', response_model=None)
    async def link_entity_to_multiple(data: LinkQueryData, token: str = Depends(oauth2_scheme)) -> None:
        check_token(token, ['admin'])
        com.link(data.dict())

    @app.patch('/entity-unlink-from-multiple', response_model=None)
    async def unlink_entity_from_multiple(data: LinkQueryData, token: str = Depends(oauth2_scheme)) -> None:
        check_token(token, ['admin'])
        com.unlink(data.dict())
    
    @app.patch('/entity-relink', response_model=None)
    async def unlink_entity_from_multiple(data: RelinkQueryData, token: str = Depends(oauth2_scheme)) -> None:
        check_token(token, ['admin'])
        com.relink(data.dict())
    
    @app.post('/entity-duplicate/{type}', response_model=Dict)
    async def duplicate_entity(type: str, data: dict, token: str = Depends(oauth2_scheme)) -> dict:
        check_token(token, ['admin'])
        return com.duplicate(type, data)
    