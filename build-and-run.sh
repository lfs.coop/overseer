#!/bin/sh

IMAGE_NAME=overseer
CONTAINER_NAME=overseer
NETWORK=dev

echo "================"
echo "CHECKING NETWORK"
echo "================"
if [ ! "$(docker network ls | grep -w ${NETWORK})" ];
  then
    echo  "- creating ${NETWORK} network!"
    docker network create ${NETWORK}
  else
    echo  "- ${NETWORK} network already exists!"
fi

echo "==============="
echo "BUILDING IMAGE: ${IMAGE_NAME}"
echo "==============="
if [ -f ./.env.production ]; then
  echo "- sourcing: '.env.production'"
  . ./.env.production
elif [ -f ./.env ]; then
  echo "- sourcing: '.env'"
  . ./.env
fi
echo "- building Docker image"
docker build -t ${IMAGE_NAME} . \
  --build-arg VUE_APP_API_HOST=${API_HOST:-0.0.0.0} \
  --build-arg VUE_APP_API_PORT=${API_PORT:-5500} \
  --build-arg VUE_APP_DEFAULT_REDIS_PASSWORD=${DEFAULT_REDIS_PASSWORD:-''} \
  --build-arg VUE_APP_DEFAULT_MONGO_PASSWORD=${DEFAULT_MONGO_PASSWORD:-''} \
  --build-arg VUE_APP_DEFAULT_EXCHANGE_SERVER_PASSWORD=${DEFAULT_EXCHANGE_SERVER_PASSWORD:-''}

echo "============="
echo "RUNNING IMAGE: ${CONTAINER_NAME}"
echo "============="
if [ "$(docker ps -aq -f name=^${CONTAINER_NAME}$)" ]; then
  docker rm -f $(docker ps -aq -f name=^${CONTAINER_NAME}$)
fi

docker run -d --rm --name ${CONTAINER_NAME} \
  -p ${API_PORT}:5500 \
  --net ${NETWORK} \
  ${IMAGE_NAME}
