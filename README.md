# Overseer

A small web app to easily manage Libreflow projects. It makes it easy to read and update their setups, and it provides various APIs to resolve a project configuration as well as all the Libreflow extensions it uses for a given site, and a given user.

**Important note: The user authentication is done via Keycloak. You must have a valid keycloak instance to use Overseer.**

## Install

To install and run Overseer, do the following:

1. Get your keycloak credentials from the keycloak admin dashboard (your client's ID, name and secret).
2. Get your Neo4j DB credentials (your username and password).
3. Copy the `.env-example` file in the repo, and rename it to `.env`.

   _Note: If you want to have some production-specific env variables, you can also create a second file named `.env.production` for overrides - it will only be used in production mode when actually building and running the Overseer app via Docker._

4. Fill the settings in the `.env` (or `.env.production`) with the credentials you retrieved earlier.
5. Generate a token secret for JWT authentication between the server and the frontend - on Unix systems, you can use:

   ```
   openssl rand -hex 32
   ```

   Then set this value in your `.env` (or `.env.production`) file for the `TOKEN_SECRET_KEY` variable.

6. Setup the remaining environment variables:
   
   - <u>App:</u> Check that the `API_HOST` and `API_PORT` are ok. Typically, for production, it is usually better to use `API_HOST="0.0.0.0"` because Overseer is packaged as a docker.
   - <u>Neo4j:</u> Check the Neo4j DB host and port info are ok with your own config. If you run Neo4j in a docker, you might need to change the `NEO4J_HOST` variable to the name of your docker container (in which case, make sure the docker runs on the same docker network as the Overseer docker).
   - <u>Keycloak:</u> You will probably need to change the `KEYCLOAK_HOST` environment variable, too. Make sure you keep the `http://<IP>:<PORT>` format! For example, if your keycloak is running in a docker, the variable will be like: `http://<LOCAL_IP_ADDRESS>:<KEYCLOAK_DOCKER_EXPOSED_PORT>`.

7. **Optional:** If you need to spin up a Neo4j docker instance, you can use the script `server/scripts/run_db.sh`. In this script, you can choose the name of the docker container to create, the docker network to create it on and the exposed Web and Bolt ports.

   Make sure your exposed Bolt port matches the `DB_PORT` variable you defined in the `.env` (or `.env.production`) file.

8. Run the Overseer app via Docker, using the `build-and-run.sh` script:

   ```
   sh build-and-run.sh
   ```

   This script will automatically source the `.env.production` file if it exists, or else the `.env` file if it exists. You can also set the name of the docker container to create, and the docker network to use.

   _**Bonus:** When you build the app, you can also specify some default values for new projects for the passwords. These are the `VUE_APP_DEFAULT_REDIS_PASSWORD`, `VUE_APP_DEFAULT_MONGO_PASSWORD` and `VUE_APP_DEFAULT_EXCHANGE_SERVER_PASSWORD` build args of the docker, which default to empty strings._

Overseer should now run as a docker on your computer - you can access it by going to the `http://<API_HOST>:<API_PORT>` URL.

## Extra options

- **Project, site and extension defaults:** To change the default values of the fields for a new entity, you can open the `client/src/helpers/models/defaults.js` file and modify these fields to your liking. The entity creation forms will be preloaded with these values, and the extension "categories" dropdown will show the default categories as quick-pick choices.

## Tools

- The database is a Neo4j NoSQL database, containing 5 entity types: `User`, `Site`, `Project`, `Extension` and `Version`.
- The backend is a Python FastAPI REST API, wrapped by [Cobblestone](https://minapecheux.gitlab.io/cobblestone)-inspired utilities for auto-routing and quick model-to-OGM creation.
- The frontend is a Vue JS app.
- The project is packaged with Docker.
