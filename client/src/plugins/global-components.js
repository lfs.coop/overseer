import Vue from 'vue';

import DuplicationPopup from '@/components/DuplicationPopup.vue';
import EntityLabel from '@/components/EntityLabel.vue';
import VTextUrl from '@/components/VTextUrl.vue';

Vue.component('DuplicationPopup', DuplicationPopup);
Vue.component('EntityLabel', EntityLabel);
Vue.component('VTextUrl', VTextUrl);
