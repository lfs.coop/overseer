import axios from 'axios';
import cloneDeep from 'lodash.clonedeep';

import { makeEntityStore } from '@/store/base-entity.js';
import { sortUsers } from '@/helpers/models/sorters';

export default makeEntityStore('user', {
  state: {
    user: null,
  },

  getters: {
    hasLocalToken: () => () => {
      const token = localStorage.getItem('__overseer__token');
      return token !== undefined && token !== null;
    },
    isAdmin: (state) => () => {
      if (!state.user) return undefined;
      return state.user.roles.includes('admin');
    },
    getUser: (state) => (login) => {
      return state.users.find((u) => u.login === login);
    },
  },

  mutations: {
    setUsers(state, { users }) { // override auto-generated to also update logged user data
      state.users = sortUsers(users);
  
      if (state.user != null) {
        const u = cloneDeep(state.user);
        u.sites = state.users.find((user) => user.uid === u.uid).sites;
        state.user = u;
      }
    },
    setMyUser(state, { user }) {
      if (user && state.users.length > 0)
        user.sites = state.users.find((u) => u.uid === user.uid).sites;
      state.user = user;
    },
  },

  actions: {
    async login({ dispatch }, { username, password }) {
      try {
        const { data } = await axios.post(
          '/token',
          { username, password },
          { headers: { 'Content-Type': 'multipart/form-data' } });
        if (data) {
          const token = data.access_token;
          localStorage.setItem('__overseer__token', token);
          return await dispatch('getCurrentUser');
        }
      } catch (err) {
        console.error(err);
        throw err;
      }
    },
    async logout({ commit }) {
      localStorage.removeItem('__overseer__token');
      commit('setMyUser', { user: null });
    },
    async getCurrentUser({ commit }) {
      const token = localStorage.getItem('__overseer__token');
      if (!token) return null;
  
      axios.defaults.headers['Authorization'] = `Bearer ${token}`;
      const { data: user } = await axios.get('/users-me');
      commit('setMyUser', { user });
      return user;
    },
    async registerKeycloakUsers({ commit }) {
      const { data: users } = await axios.patch('/register-keycloak-users');
      commit('setUsers', { users });
    },
  },

  afterDelete: async ({ commit, state }, delUid) => {
    commit('setUsers', { users: state.users.filter((x) => x.uid !== delUid) });
  },

});
