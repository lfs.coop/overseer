import cloneDeep from 'lodash.clonedeep';
import shortid from 'shortid';
import Vue from 'vue';

import { makeEntityStore } from '@/store/base-entity.js';
import { sortVersions } from '@/helpers/models/sorters.js';
import { objGet } from '@/helpers/misc.js';

export default makeEntityStore('version', {
  getters: {
    getVersionOrDefault: (state) => (uid, ext) => {
      if (uid === '__default') {
        return state.versions.find((v) => v.uid === ext.defaultVersionUid);
      }
      return state.versions.find((v) => v.uid === uid);
    },
  },

  currentTransformer: (state, v) => {
    return sortVersions(v.map((uid) => state.versions.find((v) => v.uid === uid)));
  },

  // /!\ in all mutations, no 's' to currentVersion because variable is auto-generated
  mutations: {
    pushVersions(state, { versions, pushToCurrent }) {
      state.versions = [ ...state.versions, ...versions ];
      if (pushToCurrent) {
        state.currentVersion = [ ...state.currentVersion, ...cloneDeep(versions) ];
        state.versionBackup = [ ...state.versionBackup, ...cloneDeep(versions) ];
      }
    },
    delVersions(state, { versions, pushToCurrent }) {
      state.versions = state.versions.filter((v) => !versions.includes(v.uid));
      if (pushToCurrent) {
        state.currentVersion = state.currentVersion.filter((v) => !versions.includes(v.uid));
        state.versionBackup = state.versionBackup.filter((v) => !versions.includes(v.uid));
      }
    },
    updVersions(state, { versions, pushToCurrent }) {
      const ver = cloneDeep(state.versions);
      versions.forEach((v) => {
        ver[ver.findIndex((x) => x.uid === v.uid)] = v;
      });
      state.versions = ver;
      if (pushToCurrent) {
        const curUids = state.currentVersion.map((v) => v.uid);
        state.currentVersion = cloneDeep(ver.filter((v) => curUids.includes(v.uid)));
        state.versionBackup = cloneDeep(ver.filter((v) => curUids.includes(v.uid)));
      }
    },
    addVersionToCurrent(state, d) {
      const origin = d ? objGet(d, 'origin', {}) : {};
      if ('uid' in origin) delete origin.uid;

      const version = {
        uid: shortid.generate(),
        is_enabled: true,
        name: '<new-version>',
        description: '',
        service: 'path',
        url: '',
        path: '',
        repo_project: '',
        repo_group: '',
        repo_ref_type: '',
        repo_ref: '',
        repo_token: '',
        pypi: '',
        pip_deps: '',
        env: [],
        ...origin
      };
      state.currentVersion = [ ...state.currentVersion, version ];
      state.dataIsDirty = true;
    },
    removeVersionFromCurrent(state, { version }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const versions = [ ...state.currentVersion ];
      versions.splice(idx, 1);
      state.currentVersion = versions;
      state.dataIsDirty = true;
    },
    addEnvVar(state, { version }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const v = state.currentVersion[idx];
      v.env.push({ key: '', value: '' });
      Vue.set(state.currentVersion, idx, v);
      state.dataIsDirty = true;
    },
    removeEnvVar(state, { version, index }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const v = state.currentVersion[idx];
      v.env.splice(index, 1);
      Vue.set(state.currentVersion, idx, v);
      state.dataIsDirty = true;
    },
    updateAttr(state, { version, attr, value }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const v = state.currentVersion[idx];
      v[attr] = value;
      Vue.set(state.currentVersion, idx, v);
      state.dataIsDirty = true;
    },
    updateEnvVarKey(state, { version, oldKey, key, value }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const v = state.currentVersion[idx];
      const e = v.env.find((x) => x.key === oldKey);
      e.key = key; e.value = value;
      Vue.set(state.currentVersion, idx, v);
      state.dataIsDirty = true;
    },
    updateEnvVarValue(state, { version, key, value }) {
      const idx = state.currentVersion.findIndex((x) => x.uid === version);
      const v = state.currentVersion[idx];
      v.env.find((x) => x.key === key).value = value;
      Vue.set(state.currentVersion, idx, v);
      state.dataIsDirty = true;
    },
  },

  afterDelete: async ({ commit, state }, delUid) => {
    // /!\ not used directly: versions are handled inside Extension page
    commit('version/setVersions', { versions: state.versions.filter((x) => x.uid !== delUid) });
  },

});
