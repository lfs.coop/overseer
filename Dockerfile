## STAGE 1: BUILD STATIC CLIENT
## ----------------------------
FROM node:13.10.1-alpine as build

RUN apk add g++ make py3-pip

# create app directory
RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/client
RUN mkdir -p /usr/src/app/client/src
RUN mkdir -p /usr/src/app/client/public

WORKDIR /usr/src/app/client

# install base dependencies
COPY ./client/package.json /usr/src/app/client/package.json
COPY ./client/yarn.lock /usr/src/app/client/yarn.lock
RUN yarn install --production=false --ignore-engines

# copy files
COPY ./client/src /usr/src/app/client/src
COPY ./client/public /usr/src/app/client/public
COPY ./client/babel.config.js /usr/src/app/client/babel.config.js
COPY ./client/node.config.js /usr/src/app/client/node.config.js
COPY ./client/webpack.config.js /usr/src/app/client/webpack.config.js
COPY ./client/.eslintrc.js /usr/src/app/client/.eslintrc.js

# setup env + build client
ARG VUE_APP_API_HOST=0.0.0.0
ARG VUE_APP_API_PORT=5500
ARG VUE_APP_DEFAULT_REDIS_PASSWORD=""
ARG VUE_APP_DEFAULT_MONGO_PASSWORD=""
ARG VUE_APP_DEFAULT_EXCHANGE_SERVER_PASSWORD=""

RUN yarn build --mode production

## STAGE 2: SETUP+RUN SERVER
## -------------------------
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim as production

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/client
RUN mkdir -p /usr/src/app/server

# install base dependencies
COPY ./server/requirements.txt /usr/src/app/server/requirements.txt
WORKDIR /usr/src/app/server

RUN pip install --no-cache-dir --upgrade -r requirements.txt

# copy FastAPI app, database init script and environment file
COPY --from=build /usr/src/app/client/dist /usr/src/app/client/dist
COPY ./server/scripts/init_db.py /usr/src/app/server/init_db.py
COPY ./server /usr/src/app/server

COPY ./.env.production /usr/src/app/.env

# finalize settings
CMD ["uvicorn", "server:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "5500"]
